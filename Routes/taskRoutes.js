const express = require("express");
const router = express.Router();

const taskController = require("../Controllers/taskControllers.js")

/*
	Routes
*/

// Route for getAll
router.get("/", taskController.getAll);

// Route for createTask

router.post("/addTask", taskController.createTask)

// route for deleteTask
router.delete("/deleteTasks/:id", taskController.deleteTask)

// route for getting a specific task
router.get("/:id", taskController.getOne)

// route for changing the status of a task to complete
router.put("/:id/complete", taskController.findAndUpdate)




module.exports = router;