const Task = require("../Models/task.js");


/*Controllers and functions*/

// Controller/function to gel all the task on out database
module.exports.getAll = (request, response) =>{

	Task.find({})
	// to capture the result of the find method
	.then(result => {
		return response.send(result)
	})
	// .catch method captures the error when the find method is executed.
	.catch(error => {
		return response.send(error);
	})
}

// Add Task on our Database
module.exports.createTask = (request,response) => {
	const input = request.body;

	Task.findOne({name: input.name})
	.then(result => {
		if (result !== null) {
			return response.send("The task is already existing!")
		} else {
			let newTask = new Task({
				name: input.name
			});
			newTask.save().then(save => {
				return response.send("The Task is successfully added!")
			}).catch(error => {
				return response.send(error)
			})
		}
	})
	.catch(error => {
		return response.send(error);
	})
}

// Controller that will delete the document that contains the given Object.

module.exports.deleteTask = (request, response) => {
	let idToBeDeleted = request.params.id;

	// findByIdAndRemove - to find the document that contains the id and the delete the document

	Task.findByIdAndRemove(idToBeDeleted)
	.then(result => {
		return response.send(result);
	})
	.catch(error => {
		return response.send(error);
	})
}

// controller that will get specific task

module.exports.getOne = (request, response) => {
	const id = request.params.id;

	Task.findById(id)
	.then(result => {
		return response.send(result);
	})
	.catch(error => {
		return response.send(error);
	})
}

// controller for changing the status of a task to complete
module.exports.findAndUpdate = (request, response) =>{
	const id = request.params.id;
	const input = request.body;

	Task.findByIdAndUpdate(id, {status:input.status},{new:true})
	.then(result => {
		return response.send(result);
	})
	.catch(error => {
		return response.send(error);
	})

}