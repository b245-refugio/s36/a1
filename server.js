const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./Routes/taskRoutes.js")

const app = express();
const port = 3001;


// MongoDB connection
mongoose.connect("mongodb+srv://admin:admin@batch245-refugio.uoibewa.mongodb.net/s35-discussion?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology:true
})
 
// check connection

let db = mongoose.connection;

// error catcher
db.on('error', console.error.bind(console, "Connection error!"))

// Confirmation of the connection
db.once('open', () => console.log("We are now connected to the cloud!"))


// middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// routing

app.use("/tasks", taskRoute)






app.listen(port, () => console.log(`Server is running at port ${port}!`));

/*
	Seperation of concerns :
		1.Model should be connected to the controller
		2.Controller should be connected to the routes.
		3.route should be connected to the server/application
*/